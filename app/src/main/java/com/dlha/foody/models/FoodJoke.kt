package com.dlha.foody.models

import com.google.gson.annotations.SerializedName

class FoodJoke(
    @SerializedName("text")
    val text: String
) {
}