package com.dlha.foody.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.dlha.foody.R
import com.dlha.foody.databinding.IngredientsRowLayoutBinding
import com.dlha.foody.models.ExtendedIngredient
import com.dlha.foody.util.Constants.Companion.BASE_IMAGE_URL
import com.dlha.foody.util.RecipesDiffUtil
import java.util.*


class IngredientsAdapter: RecyclerView.Adapter<IngredientsAdapter.IngredientsViewHolder>() {

    private var ingredientsList = emptyList<ExtendedIngredient>()

    inner class IngredientsViewHolder(val binding: IngredientsRowLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsViewHolder {
        return IngredientsViewHolder(IngredientsRowLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {
        holder.binding.ingredientImageView.load(BASE_IMAGE_URL + ingredientsList[position].image){
            crossfade(600)
            error(R.drawable.ic_error_placeholder)
        }
        holder.binding.ingredientNameTextView.text = ingredientsList[position].name.capitalize(
            Locale.ROOT)
        holder.binding.ingredientAmountTextView.text = ingredientsList[position].amount.toString()
        holder.binding.ingredientUnitTextView.text = ingredientsList[position].unit
        holder.binding.ingredientConsistencyTextView.text = ingredientsList[position].consistency
        holder.binding.ingredientOriginalTextView.text = ingredientsList[position].original

    }

    override fun getItemCount(): Int {
        return ingredientsList.size
    }

    fun setData(newIngredients: List<ExtendedIngredient>){
        val ingredientsDiffUtil =
            RecipesDiffUtil(ingredientsList, newIngredients)
        val diffUtilResult = DiffUtil.calculateDiff(ingredientsDiffUtil)
        ingredientsList = newIngredients
        diffUtilResult.dispatchUpdatesTo(this)
    }
}