package com.dlha.foody.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dlha.foody.models.FoodRecipe
import com.dlha.foody.util.Constants.Companion.RECIPES_TABLE

@Entity(tableName = RECIPES_TABLE)
class RecipesEntity (val foodRecipe: FoodRecipe){
    @PrimaryKey(autoGenerate = false)
    var id : Int = 0
}