package com.dlha.foody.data

import com.dlha.foody.data.network.FoodRecipesAPI
import com.dlha.foody.models.FoodJoke
import com.dlha.foody.models.FoodRecipe
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val foodRecipesAPI: FoodRecipesAPI
) {

    suspend fun getRecipes(queries: Map<String, String>): Response<FoodRecipe>{
        return foodRecipesAPI.getRecipes(queries)
    }

    suspend fun searchRecipes(searchQueries: Map<String, String>): Response<FoodRecipe>{
        return foodRecipesAPI.searchRecipes(searchQueries)
    }

    suspend fun getFoodJoke(apiKey: String): Response<FoodJoke>{
        return foodRecipesAPI.getFoodJoke(apiKey)
    }
}